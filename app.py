#!/usr/bin/python

import re
import os

import docker

client = docker.from_env()

#
# (Re)Build the Network
#

if 'nginx_aspace_proxy_net' in map(lambda _:_.name, client.networks.list()):
    print('Network "nginx_aspace_proxy_net" already exists, killing it')
    old_network = client.networks.get('nginx_aspace_proxy_net')
    for container in old_network.containers:
        old_network.disconnect(container)
    old_network.remove()

network = client.networks.create('nginx_aspace_proxy_net')

#
# Destroy the Old Nginx Container
#

if ('nginx_aspace_proxy' in map(lambda _:_.name, client.containers.list(all=True))):
    print('Container "nginx_aspace_proxy" already exists, killing it')
    old_container = client.containers.get('nginx_aspace_proxy')
    old_container.stop()
    old_container.remove()

#
# Build the nginx configurations
#

confs_dir = os.path.join(os.path.dirname(__file__), 'confs')

if not os.path.exists(confs_dir):
    os.makedirs(confs_dir)

for file in os.listdir(confs_dir):
    os.remove(os.path.join(confs_dir, file))

def process_template(servicename, template):
    containers = list(filter(
        lambda container: ('_%s_' % servicename) in container.name,
        client.containers.list()
    ))

    for container in containers:
        project = re.sub('_%s.+' % servicename, '', container.name)

        print('Project %s: Container: %s'% (project, container.name))

        network.connect(container)
        with open(os.path.join(confs_dir, '%s.conf'%container.name), 'w') as conffile:
            conffile.write(template.format(
                project=project,
                containername=container.name
            ))

#
# Map Containers in NGINX
#

process_template('aspace', """
  location /{project}/ {{
    proxy_pass http://{containername}:8081;
  }}

  location /{project}/oai {{
    proxy_pass http://{containername}:8082/;
  }}

  location /{project}/staff/ {{
    proxy_pass http://{containername}:8080;
  }}

  location /{project}/api/ {{
    proxy_pass http://{containername}:8089/;
  }}
""")

# Adminer seems to work fine, need the X-Forwarded-Prefix 
process_template('adminer', """
  location /{project}/adminer/ {{
    proxy_pass http://{containername}:8080/;
    proxy_set_header X-Forwarded-Prefix "/{project}/adminer";
  }}
""")

# Archon is a PHP Application, 
process_template('archon', """
  location /{project}/archon/ {{
    proxy_pass http://{containername}/;
    proxy_set_header X-Forwarded-Prefix "/{project}/archon";
    proxy_set_header Host $http_host;
    proxy_set_header X-Real-IP $remote_addr;
    proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    proxy_set_header X-Forwarded-Proto $scheme;
    proxy_set_header X-Original-Request $request_uri;
  }}
""")

#
# Build the nginx container
#

nginx = client.containers.run(
    'nginx', 
    detach=True, 
    name='nginx_aspace_proxy', 
    ports={'80/tcp': 80},
    network='nginx_aspace_proxy_net',
    volumes={
        os.path.join(os.path.abspath(os.path.dirname(__file__)), 'nginx.conf'): {
            'bind': '/etc/nginx/nginx.conf',
            'mode': 'ro'
        },
        os.path.abspath(confs_dir): {
            'bind': '/etc/nginx/conf.d/',
            'mode': 'ro'
        }
    },
)

client.close()

print('Done.')
